<?php 

namespace App\Database;

use Pixie\QueryBuilder\QueryBuilderHandler;
use Pixie\Connection;

class Connector 
{
    public static $qb;
    public static $alias;

    public static function PixieConnection(string $alias = '')
    {
        $config = 
            [
                'driver' => 'mysql',
                'host' => DB_HOST,
                'database' => DB_NAME,
                'username' => DB_USER,
                'password' => DB_PASSWORD,
                'charset' => DB_CHARSET,
                'collation' => DB_COLLATE,
                'prefix' => DB_PREFIX,
            ];

        if(empty(trim($alias)))
        {
            new Connection('mysql', $config);
            self::$alias = $alias;
            return;
        }
        else 
        {
            $connection = new Connection('mysql', $config, $alias);
        }

        return $connection;
    }

    public static function QBInstance()
    {
        $connection = self::PixieConnection();
        $qb = new QueryBuilderHandler($connection);
        return $qb;
    }
}
