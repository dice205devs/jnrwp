<?php 

namespace App\Services;

use App\Services\Service;

class EmailService extends Service
{
    public function __construct(string $name = '', $to = [], $message = '', $headers = '', $attachments = [])
    {
        parent::__construct($name);

        $this->to = $to;
        $this->message = $message;
        $this->headers = $headers;
        $this->attachments = $attachments;
    }

    private $to;
    private $message;
    private $headers;
    private $attachments;

    /**
     * Getters and Setters 
     */

    public function getTo()
    {
        return $this->to;
    }

    public function setTo($to)
    {
        return $this->to;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }
}
