<?php

namespace App\Services;

abstract class Service
{
    public function __construct(string $name = '')
    {
        $this->name = $name ? $name : self::class;
        $this->run();
    }

    private $name;

    /*
     * Getters and Setters
     */

    public function getServiceName(): string
    {
        return $this->name;
    }

    public function setServiceName(string $name): void 
    {
        $this->name = $name;
    }

    public abstract static function run();
}
