<?php

namespace App\Utils;

final class Utils
{
    /**
     * Nicely prints out array contents
     *
     * @param array $array - Key-value pair to display
     */
    public static function debugArray(array $array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

    /**
     * Casts a key-value pair into an object
     *
     * @param array $array - Key-value pair
     * @return $object 
     */ 
    public static function transformToObejct(array $array)
    {
        return json_decode(json_encode($array), false);
    }
}
