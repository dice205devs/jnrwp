<?php

require_once get_template_directory() . '/vendor/autoload.php';

// Initialize controllers
App\Controllers\Controller::runControllers();
