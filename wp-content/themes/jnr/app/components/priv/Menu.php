<?php

namespace App\Components\Priv;

use App\Components\Component;

abstract class Menu extends Component
{
    public function __construct(string $pageTitle = '', string $menuTitle = '', string $capability = "manage_options", $callback = null, string $slug = '')
    {
        $this->pageTitle = $pageTitle;
        $this->menuTitle = $menuTitle;
        $this->capability = $capability;
        $this->callback = $callback ? $callback : [self::class, 'render'];
        $this->slug = $slug;

        $this->load();
    }

    private $pageTitle;
    private $menuTitle;
    private $capability;
    private $callback;
    private $slug;

    /*
     * Getters and setters
     */

    public function getPageTitle(): string 
    {
        return $this->pageTitle;
    }

    public function setPageTitle(string $pageTitle): void
    {
        $this->pageTitle = $pageTitle;
    }

    public function getMenuTitle(): string
    {
        return $this->menuTitle;
    } 

    public function setMenuTitle(string $menuTitle) : void
    {
        $this->menuTitle = $menuTitle;
    }

    public function getCapability(): string
    {
        return $this->capability;
    }

    public function setCapability(string $capability): void 
    {
        $this->capability = $capability;
    }

    public function getCallback()
    {
        return $this->callback;
    }

    public function setCallback($callback): void
    {
        $this->callback = $callback;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void 
    {
        $this->slug = $slug;
    }

    public function load(): void
    {
        add_action('admin_menu', [$this, 'loadCallback']);
    }

    public function unload(): void
    {
        add_action('admin_menu', [$this, 'unloadCallback']);
    }

    public abstract function loadCallback(): void;
    public abstract function unloadCallback(): void;
}
