<?php

namespace App\Components\Priv;

use App\Components\Priv\Menu;

final class Topmenu extends Menu 
{
    public function __construct(string $pageTitle = '', string $menuTitle = '', string $capability = 'manage_options',
        array $callback = null, string $slug = '', string $menuIcon = '', int $position = 99)
    {
        parent::__construct($pageTitle, $menuTitle, $capability, $callback, $slug);

        $this->menuIcon = $menuIcon;
        $this->position = $position;
    }

    private $menuIcon;
    private $position;

    /*
     * Getters and setters 
     */ 

    public function getMenuIcon(): string 
    {
        return $this->menuIcon;
    }

    public function setMenuIcon(string $menuIcon): void 
    {
        $this->menuIcon = $menuIcon;
    }

    public function getPosition(): string 
    {
        return $this->position;
    }

    public function setPosition(string $position): void 
    {
        $this->position = $position;
    }

    public function loadCallback(): void 
    {
        add_menu_page($this->getPageTitle(), $this->getMenuTitle(), $this->getCapability(), $this->getSlug(), 
            $this->getCallback(), $this->getMenuIcon(), $this->getPosition());
    }

    public function unloadCallback(): void
    {
        remove_menu_page($this->getSlug());
    }
}
