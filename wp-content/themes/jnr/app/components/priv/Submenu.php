<?php

namespace App\Components\Priv;

use App\Components\Priv\Menu;
use App\Components\Priv\TopMenu;

final class Submenu extends Menu
{
    public function __construct(string $pageTitle = '', string $menuTitle = '', string $capability = 'manage_options',
        array $callback = null, string $slug = '', Topmenu $topMenu = null)
    {
        parent::__construct($pageTitle, $menuTitle, $capability, $callback, $slug);

        $this->topMenu = $topMenu;
    }

    /*
     * Getters and setters 
     */

    public function getTopMenu(): TopMenu 
    {
        return $this->topMenu;
    }

    public function setTopMenu(Topmenu $topMenu): void 
    {
        $this->topMenu = $topMenu;
    }

    public function loadCallback(): void
    {
        add_submenu_page($this->getTopMenu()->getSlug(), $this->getPageTitle(), $this->getMenuTitle(), $this->getCapability(),
            $this->getSlug(), $this->getCallback());
    }

    public function unloadCallback(): void
    {
        remove_submenu_page($this->getTopMenu()->getSlug(), $this->getSlug());
    }
}
