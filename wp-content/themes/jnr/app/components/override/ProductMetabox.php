<?php 

namespace App\Components\Override;

use App\Components\Priv\PostTypeMetabox;

class ProductMetabox extends PostTypeMetabox 
{
    public function render($post)
    {
        parent::render($post);

        $brand = get_post_meta($post->ID, 'brand', true);
        $old_price = get_post_meta($post->ID, 'old_price', true);
        $new_price = get_post_meta($post->ID, 'new_price', true);

        ?> 
        <!--<div class="product-metabox">
            <div class="custom_field">
                <label for="brand">Brand</label>
                <input class="regular-text" name="brand" id="brand" placeholder="Panasonic" value="<?php echo $brand; ?>"/> 
            </div>
            <div>
                <label for="old_price">Old Price</label>
                <input type="number" class="regular-text" name="old_price" id="old_price" value="<?php echo $old_price; ?>"/>
            </div>
            <div>
                <label for="new_price">New Price</label>
                <input type="number" class="regular-text" name="new_price" id="new_price" value="<?php echo $new_price; ?>"/>
            </div>
        </div>-->
        <div>
            <div class="form-group">
                <label for="brand">Brand</label>
                <input type="text" class="form-control" name="brand" id="brand" value="<?php echo $brand; ?>"/>
            </div>
            <div class="form-group">
                <label for="old_price">Old Price</label>
                <input type="number" class="form-control" name="old_price" id="old_price" value="<?php echo $old_price; ?>"/>
            </div>
            <div class="form-group">
                <label for="new_price">New Price</label>
                <input type="number" class="form-control" name="new_price" id="new_price" value="<?php echo $new_price; ?>"/>
            </div>
        </div>
        <?php
    }

    public function save($post_id)
    {
        parent::save($post_id);

        $brand = isset($_POST['brand']) ? sanitize_text_field($_POST['brand']) : '';
        $old_price = isset($_POST['old_price']) ? sanitize_text_field($_POST['old_price']) : '';
        $new_price = isset($_POST['new_price']) ? sanitize_text_field($_POST['new_price']) : '';

        update_post_meta($post_id, 'brand', $brand);
        update_post_meta($post_id, 'old_price', $old_price);
        update_post_meta($post_id, 'new_price', $new_price);
    }
}
