<?php 

namespace App\Components\Pub;

use App\Components\Component;

class PostType extends Component 
{
    public function __construct(string $name = '', array $args = [])
    {
        parent::__construct($name);
        $this->args = $args;
    }

    // Getters and setters 

    public function getArgs(): array 
    {
        return $this->args;
    }

    public function setArgs(array $args): void 
    {
        $this->args = $args;
    }

    public function load(): void
    {
        add_action('init', [$this, 'loadCallback']);
    }

    public function loadCallback(): void
    {
        if(!array_key_exists('label', $this->getArgs()))
        {
            $args = $this->getArgs();
            $args['label'] = ucfirst($this->getName());
            $this->setArgs($args);
        }

        register_post_type($this->getName(), $this->getArgs());
    }

    public function unload(): void
    {
        add_action('init', [$this, 'unloadCallback']);
    }

    public function unloadCallback(): void
    {
        unregister_post_type($this->getName());
    }
}
