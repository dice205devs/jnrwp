<?php 

namespace App\Components\Pub;

use App\Components\Component;
use App\Components\Pub\PostType;

class Taxonomy extends Component 
{
    public function __construct(string $name = '', array $args = [], PostType $postType = null)
    {
        parent::__construct($name);
        $this->args = $args;
        $this->postType = $postType;
    }

    private $args;
    private $postType;

    // Getters and setters 

    public function getArgs(): array 
    {
        return $this->args;
    }

    public function setArgs(array $args): void
    {
        $this->args = $args;
    }

    public function getPostType(): PostType 
    {
        return $this->postType;
    }

    public function setPostType(PostType $postType): void 
    {
        $this->postType = $postType;
    }

    public function load(): void
    {
        add_action('init', [$this, 'loadCallback']);
    }

    public function loadCallback(): void
    {
        register_taxonomy($this->getName(), $this->getPostType()->getName(), $this->getArgs());
        register_taxonomy_for_object_type($this->getName(), $this->getPostType()->getName());
    }

    public function unload(): void
    {
        add_action('init', [$this, 'unloadCallback']);
    }

    public function unloadCallback(): void
    {
        unregister_taxonomy_for_object_type($this->getName(), $this->getPostType()->getName());
    }
}
