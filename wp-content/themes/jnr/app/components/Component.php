<?php 

namespace App\Components;

/**
 * Base class of all components 
 */

abstract class Component 
{
    public function __construct(string $name = '')
    {
        $this->name = $name;
        $this->load();
    }

    private $name;

    // Getters and setters 

    public function getName(): string 
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    // Component 

    public abstract function load(): void;
    public abstract function loadCallback(): void;
    public abstract function unload(): void;
    public abstract function unloadCallback(): void;
}
