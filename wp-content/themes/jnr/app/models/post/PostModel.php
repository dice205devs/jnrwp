<?php 

namespace App\Models\Post;

use App\Models\ModelInterface;

abstract class PostModel implements ModelInterface
{
    protected static $postType = '';

    public static function all()
    {
        global $wpdb;

        $post_type = static::$postType;

        $rows = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_type = '$post_type'");
        return $rows;
    }

    public static function find(array $args)
    {

    }

    public static function findOne(int $id)
    {

    }

    public static function delete(array $args)
    {

    }

    public static function deleteOne(int $id)
    {
        global $wpdb;

    }

    public static function update(array $args)
    {

    }

    public static function updateOne(int $id)
    {

    }
}
