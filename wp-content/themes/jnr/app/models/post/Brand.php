<?php 

namespace App\Models\Post;

use App\Models\Post\PostModel;

class Brand extends PostModel 
{
    protected static $postType = 'brand';
}
