<?php 

namespace App\Models;

use App\Models\Plain\Model;

class ContactForm extends Model
{
    protected static $table = 'contact_form';
}
