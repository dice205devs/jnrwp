<?php

namespace App\Models\Plain;

use App\Database\Connector;
use App\Models\ModelInterface;

abstract class Model implements ModelInterface
{
    protected static $table ;

    public static function all()
    {
        $qb = Connector::QBInstance('QB');
        $rows = $qb->table(static::$table)->get();
        return $rows;
    }

    public static function find(array $args)
    {
        $qb = Connector::QBInstance();
    }

    public static function findOne(int $id)
    {
        $qb = Connector::QBInstance();
        $row = $qb->table(static::$table)->find($id);
        return $row;
    }

    public static function delete(array $args)
    {
        $qb = Connector::QBInstance();
    }

    public static function deleteOne(int $id)
    {
        $qb = Connector::QBInstance();
    }

    public static function update(array $args)
    {
        $qb = Connector::QBInstance();
    }

    public static function updateOne(int $id)
    {
        $qb = Connector::QBInstance();
    }
}
