<?php namespace App\Models;

interface ModelInterface
{
    public static function all();

    public static function find(array $args);

    public static function findOne(int $id);

    public static function delete(array $args);

    public static function deleteOne(int $id);

    public static function update(array $args);

    public static function updateOne(int $id);
}
