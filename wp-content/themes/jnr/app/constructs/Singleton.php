<?php 

namespace App\Constructs;

class Singleton
{
    private function __construct()
    {

    }

    private static $instance = null;

    public static function Instance()
    {
        if(self::$instance === null)
        {
            self::$instance = new static();
        }

        return self::$instance;
    }
}
