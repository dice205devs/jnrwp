<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Components\Pub\PostType;
use App\Components\Pub\Taxonomy;
use App\Components\Override\ProductMetabox;
use App\Database\Connector;
use App\Utils\Utils;

final class ProductController extends Controller
{
    public static function run()
    {
        $cpt = self::register_cpt();
        self::register_taxonomy($cpt);
        self::register_metabox($cpt);
    }

    private static function register_cpt()
    {
        $post_type = new PostType();
        $post_type->setName('product');
        $post_type->setArgs([
            'public' => true,
            'label' => 'Products',
            'menu_icon' => 'dashicons-cart',
            'supports' =>
            [
                'title',
                'editor',
                'thumbnail',
            ]
        ]);
        return $post_type;
    }

    private static function register_taxonomy(PostType $post_type)
    {
        $tax = new Taxonomy();
        $tax->setName('product_category');
        $tax->setArgs([
            'label' => 'Categories',
            'hierarhical' => true,
        ]);
        $tax->setPostType($post_type);

        return $tax;
    }

    private static function register_metabox(PostType $post_type)
    {
        $product_metabox = new ProductMetabox();
        $product_metabox->setName('product_metabox');
        $product_metabox->setTitle('Product Details');
        $product_metabox->setScreen($post_type);

        return $product_metabox;
    }
}
