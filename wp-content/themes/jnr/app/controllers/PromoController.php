<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Components\Pub\PostType;
use App\Components\Pub\Taxonomy;

final class PromoController extends Controller
{
    public static function run()
    {
        $cpt = static::register_cpt();
        static::register_taxonomy($cpt);
    }

    private static function register_taxonomy($post_type)
    {
        $tax = new Taxonomy();
        $tax->setName('promo_taxonomy');
        $tax->setArgs([
            'public' => true,
            'hierarchical' => true,
            'label' => 'Promo Categories'
        ]);
        $tax->setPostType($post_type);
    }

    private static function register_cpt()
    {
        $post_type = new PostType();
        $post_type->setName('promo');
        $post_type->setArgs([
            'public' => true,
            'show_in_rest' => true,
            'label' => 'Promos',
            'menu_icon' => 'dashicons-megaphone'
        ]);

        return $post_type;
    }
}
