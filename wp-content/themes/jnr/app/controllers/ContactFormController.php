<?php

namespace App\Controllers;

use App\Components\Priv\Topmenu;
use App\Components\Priv\Submenu;
use App\Controllers\Controller;
use App\Utils\Utils;
use App\Models\ContactForm;

final class ContactFormController extends Controller
{
    const ACTION_NAME = "contact_form_submit";

    /**
     * Entry point of the controller. Everything included in this function will 
     * run
     *
     * @uses ContactFormController::render()
     * @uses ContactFormController::handleSubmit()
     */
    public static function run()
    {
        add_shortcode('renderForm', [self::class, 'render']);

        add_action("admin_post_" . self::ACTION_NAME, [self::class, 'handleSubmit']);
        add_action("admin_post_nopriv_" . self::ACTION_NAME, [self::class, 'handleSubmit']);

        self::registerMenu();
    }

    /**
     * Renders an HTML form
     *
     * Note that rather than running this function via its class, it is intended to be used as a shortcode. The
     * name of the shortcode can be seen in the class constructor
     */
    public static function render($atts)
    {
        /* do_shortcode('[renderForm title="Nibba"]'); */
        ?>
        <form action="<?php echo get_admin_url() . 'admin-post.php'; ?>" method="post">
            <input type="hidden" name="action" value="contact_form_submit"/>
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" placeholder="Name" required/>
            </div>
            <div class="form-group">
                <input type="text" name="contact-number" class="form-control" placeholder="Contact Number" required/>
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" required/>
            </div>
            <div class="form-group">
                <textarea name="message" class="form-control" placeholder="Message" required></textarea>
            </div>
            <button class="btn btn-primary">Submit</button>
        </form>
        <?php
    }

    /**
     * Handles the submission of the form
     */
    public static function handleSubmit()
    {
        $post_obj = Utils::transformToObejct($_POST);
        $name = sanitize_text_field($post_obj->name);
        $email = sanitize_email($post_obj->email);
        $message = sanitize_textarea_field($post_obj->message);
    }

    /**
     * Registers WP Menu items
     */
    public static function registerMenu()
    {
        $menu = new Topmenu();
        $menu->setPageTitle('Contact Form Submissions');
        $menu->setMenuTitle('Contact Form Submissions');
        $menu->setCallback([self::class, 'renderMenuPage']);
        $menu->setSlug("contact-form-submissions");
        $menu->setMenuIcon('dashicons-feedback');

        $submenu = new Submenu();
        $submenu->setPageTitle('Settings');
        $submenu->setMenuTitle('Settings');
        $submenu->setCallback([self::class, 'renderSubmenuPage']);
        $submenu->setTopMenu($menu);
    }

    public static function renderMenuPage()
    {
        $submissions = ContactForm::all();
        ?>
        <h1>Contact Form Submissions</h1>
        <div class="widefat">
            <table id="contactSubmissions" class="alternate custom-table dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Number</th>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
                    foreach($submissions as $submission)
                    {
                        ?> 
                        <td><?php echo $submission->name; ?></td>
                        <td><?php echo $submission->email; ?></td>
                        <td><?php echo $submission->number; ?></td>
                        <td><?php echo $submission->email; ?></td>
                        <td><?php echo $submission->message; ?></td>
                        <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Number</th>
                        <th>Message</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <?php
    }

    public static function renderSubmenuPage()
    {
        ?>
        <h1>Submenu</h1>
        <?php
    }
}
