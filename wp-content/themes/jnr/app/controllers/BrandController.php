<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Components\Pub\PostType;

class BrandController extends Controller
{
    public static function run()
    {
        $cpt = self::register_cpt();
    }

    private static function register_cpt()
    {
        $post_type = new PostType();
        $post_type->setName('brand');
        $post_type->setArgs([
            'public' => true,
            'label' => 'Brands',
            'menu_icon' => 'dashicons-universal-access',
            'supports' => [
                'title',
                'editor',
                'thumbnail'
            ]
        ]);
        return $post_type;
    }
}
