<?php

namespace App\Controllers;

abstract class Controller
{
    abstract public static function run();

    public static function runControllers()
    {
        $exclude_files = [
            ".",
            "..",
            "Controller.php",
        ];

        $controller_dirs = [
            "\App\Controllers\\" => get_template_directory() . '/app/controllers/'
        ];

        try 
        {
            foreach($controller_dirs as $namespace => $directory)
            {
                $files = scandir($directory);
                foreach($files as $file)
                {
                    if(!in_array($file, $exclude_files))
                    {
                        $without_extension = pathinfo($file, PATHINFO_FILENAME);
                        ($namespace . '' . $without_extension)::run();
                    }
                }
            }
        } 
        catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }
}
