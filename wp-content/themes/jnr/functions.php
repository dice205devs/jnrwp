<?php
require_once __DIR__ . '/app/start.php';

add_action('wp_enqueue_scripts', 'enqueue_scripts');
add_action('admin_enqueue_scripts', 'admin_scripts');
add_action('after_setup_theme', 'theme_setup');
add_action('init', 'define_constants');
add_theme_support('post-thumbnails');
add_action('after_switch_theme', 'prepare_database_tables');

function enqueue_scripts()
{
    $node_modules_dir = get_template_directory_uri() . '/node_modules/';

    wp_enqueue_style('jnr_bootstrap_css', $node_modules_dir . 'bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_style('jnr_css', JNR_CSS . 'style.css');

    wp_deregister_script('jquery');
    wp_enqueue_style('jnr_bootstrap_css', $node_modules_dir . 'bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_script('jnr_jquery', $node_modules_dir . 'jquery/dist/jquery.min.js');
    wp_enqueue_script('jnr_popper.js', $node_modules_dir . 'popper.js/dist/umd/popper.js');
    wp_enqueue_script('jnr_bootstrap_js', $node_modules_dir . 'bootstrap/dist/js/bootstrap.min.js', ['jnr_jquery', 'jnr_popper.js']);
}

function admin_scripts()
{
    $admin_style_dir = get_template_directory_uri() . '/assets/admin/';

    wp_enqueue_style('jnr_admin_css', $admin_style_dir . 'css/style.css');
    wp_enqueue_script('jnr_admin_js', $admin_style_dir . 'js/script.js');

    // Admin screens where bootstrap must be enqueued
    $bootstrap_screens = [
        /* 'product' */
    ];

    if(in_array(get_current_screen()->id, $bootstrap_screens))
    {
        $node_modules_dir = get_template_directory_uri() . '/node_modules/';
        wp_enqueue_style('jnr_bootstrap_css', $node_modules_dir . 'bootstrap/dist/css/bootstrap.min.css');
        wp_enqueue_script('jnr_jquery', $node_modules_dir . 'jquery/dist/jquery.min.js');
        wp_enqueue_script('jnr_popper.js', $node_modules_dir . 'popper.js/dist/umd/popper.js');
        wp_enqueue_script('jnr_bootstrap_js', $node_modules_dir . 'bootstrap/dist/js/bootstrap.min.js', ['jquery', 'jnr_popper.js']);
    }
}

function theme_setup()
{
    $defaults = [
        'height' => 100,
        'width' => 400,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => ['site-title', 'site-description'],
    ];
    add_theme_support('custom-logo', $defaults);
}

function define_constants()
{
    define('JNR_ASSETS', get_template_directory_uri() . '/assets/');
    define('JNR_CSS', get_template_directory_uri() . '/assets/css/');
    define('JNR_JS', get_template_directory_uri() . '/assets/js/');
    define('JNR_IMG', get_template_directory_uri() . '/assets/img/');
}


function prepare_database_tables()
{
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $tables = [
        "{$wpdb->prefix}contact_form" => "
        CREATE TABLE {$wpdb->prefix}contact_form  (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        email varchar(255) NOT NULL,
        number varchar(15) NOT NULL,
        message text NOT NULL,
        created_at date NOT NULL,
        checked tinyint(1) DEFAULT 0,
        PRIMARY KEY  (id)
        ) $charset_collate;
",
        "$wpdb->prefix"
    ];

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    foreach($tables as $table => $sql)
    {
        dbDelta($sql);
    }
}
