jQuery(document).ready(function($) {
    $('#contactSubmissions').DataTable({
        "order":[[0,"desc"]],
        "pageLength": 10,
        dom: 'Bfrtip',
        buttons: [
            //'copy',
            {
                extend: 'csv',
                text: 'Export as CSV',
                filename: 'umtc-contact-form-submissions',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            // {
            //     extend: 'csv',
            //     text: 'Save shown',
            //     filename: 'umtc-filtered-contact-form-submissions',
            // }

        ]
    });
});

